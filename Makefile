PREFIX = /usr/local
#CPPFLAGS = -DDEBUG
LDLIBS = -lcec
OBJS = ceckb.o
BIN = ceckb

all: $(BIN)

ceckb.o: config.h

install: all
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f $(BIN) $(DESTDIR)$(PREFIX)/bin

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/$(BIN)

clean:
	rm -f $(OBJS) $(BIN)

config.h:
	cp config.def.h $@

.PHONY: all clean install uninstall
